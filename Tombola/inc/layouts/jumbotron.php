<div class="jumbotron jumbotron-fluid mb-3">
  <div class="container">
    <!-- Si on est sur la page d'accueil ou la page d'achat de tickets -->
    <?php if(!isset($_GET['results'])):?>
    <p class="lead">Vous allez participer à la tombola n°<strong><?=$_SESSION['tombola']['session']?></strong> du Lundi de validation !!</p>
    <p>Le prix du ticket de tombola est de <strong>2€</strong>, vous pouvez en acheter un maximum de <strong>100</strong> (auquel cas vous serez le seul joueur...).</p>
    <p>Les prix seront les suivants :</p>
    <ol>
      <li>100€</li>
      <li>50€</li>
      <li>20€</li>
    </ol>
    <?php endif; ?>
    <!-- Si on est sur la page des résultats -->
    <?php if(isset($_GET['results'])):?>
    <p class="lead">Vous venez participer à la tombola n°<?=$_SESSION['tombola']['session']?> du Lundi de validation !!</p>
    <p>Voyons les résultats !!</p>
    <?php endif; ?>
    <a class="btn btn-danger" href="?restart">Repartir de zér0</a>
    <!-- Permet d'afficher le bouton commencer seulement si on est sur la page d'acceuil -->
    <?php if(!isset($_GET['purchase'])): ?>
      <?php if(!isset($_POST['nbTickets'])): ?>
        <?php if(!isset($_GET['results'])): ?>
        <a href="?purchase" class="btn btn-primary">Commencer</a>
        <?php endif;?>
      <?php endif;?>
    <?php endif;?>
  </div>
</div>
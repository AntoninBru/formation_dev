<div class="container">
  <div class="bg-dark text-light p-3 mx-auto col-5">
    <h4 class="text-center">Les résultats sont les suivants :</h4>
        <p class="text-center"><strong>Premier prix :</strong> Ticket n°<?=$_SESSION['tombola']['winnerTickets'][0];?> (100€)</p>
        <p class="text-center"><strong>Second prix :</strong> Ticket n°<?=$_SESSION['tombola']['winnerTickets'][1];?> (50€)</p>
        <p class="text-center"><strong>Troisième prix :</strong> Ticket n°<?=$_SESSION['tombola']['winnerTickets'][2];?> (20€)</p>
    </p> 
    <p class="text-center">Vous avez gagné : <strong><?=$_SESSION['tombola']['playerPrice'];?></strong>€</p>
    <p class="text-center">Il vous reste : <strong><?=$_SESSION['tombola']['money'];?></strong>€</p>
    <p class="text-center"><a href="?newTombola" class="btn btn-warning" >Retenter ma chance</a></p>

  </div>
  <?php if(isset($_SESSION['tombola']['playerTickets'])){
    include_once'./inc/layouts/ticketsTable.php';
  } ?>
  
</div>

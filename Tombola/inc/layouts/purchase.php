<div class="container">
  <div class="bg-dark text-light p-3 mx-auto col-5">
    <p class="text-center">Vous disposez de : <strong><?=$_SESSION['tombola']['money']?></strong>€</p>
    <p class="text-center">Combien voulez vous de tickets?</p>
    <form action="index.php" method="post" class="text-center mx-auto col-10">
      <input type="number" class="form-control" name="nbTickets">
      <input type="submit" name="submit" class="btn btn-primary col-6 my-3" value="Acheter">
      <!-- Affiche l'erreur si elle existe -->
      <?php if(isset($error)):?>
        <div class="alert alert-danger" role="alert">
          <?=$error;?>
        </div>
      <?php endif; ?>
    </form>
    <!-- Si on a acheter au moins 1 ticket alors on peux procéder au tirage -->
    <?php if(isset($_POST['nbTickets'])): ?>
      <p class="text-center mb-0"><a href="?results" class="btn btn-success my-1 mx-auto">Lancer le tirage</a><p>
    <?php endif;?>
  </div>
  <!-- Si on a acheté des ticket on inclut le tableau d'affichage des tickets -->
  <?php if(isset($_SESSION['tombola']['playerTickets'])){
    include_once'./inc/layouts/ticketsTable.php';
  } ?>
  
</div>
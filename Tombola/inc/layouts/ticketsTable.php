<div class="col-3 mx-auto mt-3 text-center" >
  <a class="btn btn-info " data-toggle="collapse" href="#table" role="button" aria-expanded="false" aria-controls="collapseExample">
    Voir vos <?php echo count($_SESSION['tombola']['playerTickets']);?> tickets
  </a>
</div>
<table class="table table-dark table-striped bg-dark table-hover text-center table-sm col-3 mx-auto mt-3">
  <thead>
    <tr>
      <th scope="col">Tickets</th>
    </tr>
  </thead>
  <tbody class="collapse" id="table">
  <?php foreach($_SESSION['tombola']['playerTickets'] as $val): ?>
    <tr><td><?=$val;?></td></tr>
  <?php endforeach;?>
  </tbody>
</table>
<?php
/**
 * Fonction d'achat de tickets
 *
 * @param [type] $nbTicket Le nombre de ticket à acheter
 * @return void
 */
function purchaseTicket($nbTicket){
  //si la valeur est numérique et si le nombre de ticket demandé est compris entre 1 et 100 et que le nombre de ticket du joueur n'est pas déjà de 100 alors on peux procéder a la sélection de tickets
  if((is_numeric($nbTicket))&&(($nbTicket>=1)&&($nbTicket<=100))){
    if(isset($_SESSION['tombola']['playerTickets'])){
      $playerTickets = count($_SESSION['tombola']['playerTickets']);
      if($playerTickets >= 100){
        return 'Vous possédez déjà les 100 Tickets';
      }
    }
    //on fera autant de tours de boucle que le nombre de ticket demandé
    for($i=0; $i <$nbTicket; $i++){
      //Si le joueur a assez d'argent alors on fait un tour de boucle
      if($_SESSION['tombola']['money']>0){
        //on sélectionne un nombre aléatoirement entre 1 et 100
        $ticket = mt_rand(1,100);
        //Si le joueur possède des ticket on vérifie si il y en a un qui possède déjà la valeur sélectionnée au dessus
        if(isset($_SESSION['tombola']['playerTickets'])){
          //tant que la valeur sélectionné existe dans le tableau on en recherche une nouvelle
          while(in_array($ticket,$_SESSION['tombola']['playerTickets'])&&(count($_SESSION['tombola']['playerTickets'])<100)){
            $ticket = mt_rand(1,100);
          }
        }
        //Après vérifications on donne le ticket au joueur 
        $_SESSION['tombola']['playerTickets'][] = $ticket;
        //Le joueur paye le prix du ticket
        $_SESSION['tombola']['money'] -= 2;
      } else {
        //Si le joueur n'a plus assez d'argent on sort de la boucle
        return 'Vous n\'avez plus d\'argent';
        break;
      }

    }
    
  } else {
    //Si l'erreur est que le nombre de ticket demandé n'est pas compris entre 1 et 100
    if(!is_numeric($nbTicket)){
      return 'Vous devez rentrer un nombre!';
    }
    //Si l'erreur est que le nombre de ticket demandé n'est pas compris entre 1 et 100
    if(($nbTicket<1)||($nbTicket>100)){
      return 'Vous ne pouvez prendre qu\'entre 1 et 100 Tickets';
    }
    //Si l'erreur est que le joueur possède déjà 100 tickets
    if(isset($_SESSION['tombola']['playerTickets'])&&(count($_SESSION['tombola']['playerTickets'])==100)){
      return 'Vous possédez déjà les 100 Tickets';
    }
  }
}

/**
 * Fonction de tirage des tickets gagnants
 *
 * @return un array avec les 3 tickets gagnants
 */
function winnerTicket(){
  // On initialise l'array tickets
  $tickets = [];
  // On fait 3 tours de boucle
  for($i=0; $i < 3; $i++){
    // On tire un ticket au sort et on le stocke
    $ticket = mt_rand(1,100);
    // On vérifie si l'index choisit n'est pas déjà dans l'array. Si il y est on en tire un nouveau
    while(in_array($ticket,$tickets)){
      $ticket = mt_rand(1,100);
    }
    // Après vérifications on stock le ticket
    $tickets[] = $ticket;
  }
  return $tickets;
}

/**
 * Fonction de vérification des tickets
 *
 * @param array $winnerTickets Nos ticket gagnats
 * @return Total des gains
 */
function checkTicket($winnerTickets){
  $playerPrice = 0;
  for($i = 0; $i < count($winnerTickets); $i++){
    if(in_array($winnerTickets[$i],$_SESSION['tombola']['playerTickets'])){
      $playerPrice += PRICES[$i];
    }
  }
  $_SESSION['tombola']['money'] += $playerPrice;
  return $playerPrice;
}

/**
 * Fonction de réinitialisation de $_SESSION['tombola']
 *
 * @return void
 */
function restart(){
  // On vide la $_SESSION['tombola']
  unset($_SESSION['tombola']);
  // On vas réactualiser la page
  $page = header('Location: '. $_SERVER['PHP_SELF']);
  // On arrète l'exécution du script PHP
  exit();
}

/**
 * Fonction de lancement d'une nouvelle tombola
 *
 * @return void
 */
function newTombola(){
  // On vide : Les ticket gagnants, gains du joueur, nombre de ticket à tirer et tickets du joueur
  unset($_SESSION['tombola']['winnerTickets']);
  unset($_SESSION['tombola']['playerPrice']);
  unset($_SESSION['tombola']['nbTickets']);
  unset($_SESSION['tombola']['playerTickets']);
  // On incrémante la session de tombola
  $_SESSION['tombola']['session'] += 1;
  $page = header('Location: '. $_SERVER['PHP_SELF'].'?purchase');
  exit();
}
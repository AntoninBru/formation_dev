<?php
session_start();
require_once'./inc/functions.php';

define('PRICES',[100,50,20]);

//Au départ on initialise le portefeuille du joueur à 500€
if(!isset($_SESSION['tombola']['money'])){
  $_SESSION['tombola']['money'] = 500;
}

//Au départ c'est la première session de tombola
if(!isset($_SESSION['tombola']['session'])){
  $_SESSION['tombola']['session'] = 1;
}

//Si $_POST['nbTickets'] existe alors on vas acheter nos tickets en cas d'erreur $error retourneras cette dernière
if(isset($_POST['nbTickets'])){
  $error = purchaseTicket($_POST['nbTickets']);
}

// Si on veux les résultats alors on procède au tirage puis on vérifie si on a les tickets gagnants
if(isset($_GET['results'])&&(!isset($_SESSION['tombola']['winnerTickets']))){
  $_SESSION['tombola']['winnerTickets']= winnerTicket();
  $_SESSION['tombola']['playerPrice'] = checkTicket($_SESSION['tombola']['winnerTickets']);
}

// Si on veux repartir de zéro alors on lance restart
if(isset($_GET['restart'])){
  restart();
}

// Si on lance newTombola alors on procède à une nouvelle session de tombola
if(isset($_GET['newTombola'])){
  newTombola();
}

include_once'./inc/view.php';
